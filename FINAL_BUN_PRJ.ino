int WATERPUMP = 12; //motor pump connected to pin 12
int sensor = A1; //sensor analog pin connected to pin A1
int val; //This variable stores the value received from Soil moisture sensor.

void setup() {
  pinMode(8, INPUT);
  pinMode(12, OUTPUT); //Set pin 12 as OUTPUT pin
  pinMode(A1, INPUT); //Set pin A1 as input pin, to receive data from Soil moisture sensor.
  //Initialize serial and wait for port to open:
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  while (! Serial);
  Serial.println("Am inceput programul:");
}

void loop() {
  int temp = digitalRead(8); 
  int  val = analogRead(A1);
  Serial.print("umiditate: "); Serial.println(val);
  //Read data from soil moisture sensor
  if (temp == LOW)
  { Serial.println("Este zi/lumina!");
    if (val <= 1023 && val >= 500)
    {
      Serial.println("Sol uscat. Udam 3 secunde");
      digitalWrite(12, HIGH); 
      delay(3000);
      digitalWrite(12, LOW);
    }
    else if (val < 500)
    {
      Serial.println("Sol umed. Nu udam");
      digitalWrite(12, LOW); 
      }
  }
  else
  {
    Serial.println("Este noapte/intuneric!");
  }

  Serial.println("Final colectare date. Le re-verificam peste 10 secunde.");

  delay(10000); //Wait for few second and then continue the loop.
}
